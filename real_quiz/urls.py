from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^', include('campaign.urls', namespace="campaign")),
    url(r'^admin/', admin.site.urls),
]
