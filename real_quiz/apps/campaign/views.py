import haikunator
from django.shortcuts import render
from django.db import transaction
from django.views.generic import (
    TemplateView,
)

from .models import Room


class SiteIndexView(TemplateView):
    """
    Default Site Index
    """
    template_name = "campaign/home.html"

    def get_context_data(self, **kwargs):
        context = super(SiteIndexView, self).get_context_data(**kwargs)
        new_room = None
        while not new_room:
            with transaction.atomic():
                label = haikunator.haikunate()
                if Room.objects.filter(label=label).exists():
                    continue
                new_room = Room.objects.create(label=label)
                context['label'] = label
        return context


class NewGameView(TemplateView):
    """
    Default Site Index
    """
    template_name = "campaign/new-game.html"

    def get_context_data(self, **kwargs):
        context = super(NewGameView, self).get_context_data(**kwargs)
        room, created = Room.objects.get_or_create(label=kwargs.get('label'))
        messages = reversed(room.messages.order_by('-timestamp')[:50])
        context['room'] = room
        context['messages'] = messages

        return context
