from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView

from . import views


urlpatterns = [
    url(
        regex=r'^$',
        view=views.SiteIndexView.as_view(),
        name="home"
    ),
    url(
        regex=r'^(?P<label>[\w-]{,50})/$',
        view=views.NewGameView.as_view(),
        name='new_game'
    ),
]
